# GameSparks Java Client SDK

- This is the gamesparks Java client SDK, used in the GameSparks Android SDK.

- This SDK can be used in any Java project.

- Details of usage can be found at [https://docs.gamesparks.net/sdk/android-sdk-setup](https://docs.gamesparks.net/sdk/android-sdk-setup)