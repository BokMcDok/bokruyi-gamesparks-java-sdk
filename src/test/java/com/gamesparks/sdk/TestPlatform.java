package com.gamesparks.sdk;

import java.io.File;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class TestPlatform implements IGSPlatform {

	private String playerId;
	private String authToken;
	
	@Override
	public File getWritableLocation() {
		return new File(System.getProperty("java.io.tmpdir"));
	}

	@Override
	public void executeOnMainThread(Runnable job) {
		job.run();
	}

	@Override
	public String getPlayerId() {
		return playerId;
	}

	@Override
	public String getAuthToken() {
		return authToken;
	}

	@Override
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	@Override
	public void setAuthToken(String authToken) {
		logMessage("Got authtoken " + authToken);
		
		this.authToken = authToken;
	}

	@Override
	public String getHmac(String data, String secret) {
		try {
			Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA256");
			
			sha256_HMAC.init(secret_key);
			
			return Base64.getEncoder().encodeToString(sha256_HMAC.doFinal(data.getBytes("UTF-8")));
		} catch(Exception e) {
			return null;
		}

	}

	@Override
	public void logMessage(String msg) {
		System.out.println(msg);
		
	}

	@Override
	public void logError(Throwable t) {
		t.printStackTrace();
	}

}
