package com.gamesparks.sdk;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gamesparks.sdk.api.GSData;
import com.gamesparks.sdk.api.autogen.GSMessageHandler;
import com.gamesparks.sdk.api.autogen.GSRequestBuilder;
import com.gamesparks.sdk.api.autogen.GSMessageHandler.ScriptMessage;
import com.gamesparks.sdk.api.autogen.GSRequestBuilder.AuthenticationRequest;
import com.gamesparks.sdk.api.autogen.GSRequestBuilder.LogEventRequest;
import com.gamesparks.sdk.api.autogen.GSResponseBuilder.AroundMeLeaderboardResponse;
import com.gamesparks.sdk.api.autogen.GSResponseBuilder.AuthenticationResponse;
import com.gamesparks.sdk.api.autogen.GSResponseBuilder.LogEventResponse;
import com.gamesparks.sdk.api.autogen.GSTypes.LeaderboardData;

import com.gamesparks.sdk.tests.*;

public class Main {
	
	private static GSExtended mCurrInstance;
	
	public static void part2() {
		LogEventRequest request = mCurrInstance.getRequestBuilder().createLogEventRequest()
				.setEventKey("001")
				.setEventAttribute("0001", "AAAH");

		request.setDurable(true);
		request.send(new GSEventConsumer<LogEventResponse>() {
			@Override
			public void onEvent(LogEventResponse logEventResponse) {
				try {
					GSData scriptData = logEventResponse.getScriptData();
					System.out.println("numberList: " + scriptData.getNumberList("numberList"));
					System.out.println("booleanList: " + scriptData.getBooleanList("booleanList"));
					System.out.println("stringList: " + scriptData.getStringList("stringList"));
				} catch (Exception e) {
					System.out.println(e.toString());
				}
			}
		});
	}
	
	public static void part1() {
		try {
			GSHelperMethods.waitForDeviceAuthentication(mCurrInstance, new GSHelperMethods.Callback() {
				@Override
				public void function() {
					try {
						part2();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		try {
			DurableNamedInstance test1 = new DurableNamedInstance();
			
			while (true) {
				if (test1.mFinished) {
					break;
				} else {
					Thread.sleep(100);
				}
			}
			
			DurableReconnect test2 = new DurableReconnect();
			
			while (true) {
				if (test2.mFinished) {
					break;
				} else {
					Thread.sleep(100);
				}
			}
			
			PauseDurableQueue test3 = new PauseDurableQueue();
			
			while (true) {
				if (test3.mFinished) {
					break;
				} else {
					Thread.sleep(100);
				}
			}
			
			RestartDurableQueue test4 = new RestartDurableQueue();
			
			while (true) {
				if (test4.mFinished) {
					break;
				} else {
					Thread.sleep(100);
				}
			}
			
			IterateDurableEntries test5 = new IterateDurableEntries();
			
			while (true) {
				if (test5.mFinished) {
					break;
				} else {
					Thread.sleep(100);
				}
			}
			
			/*CheckDurableSendOrder test6 = new CheckDurableSendOrder();
			
			while (true) {
				if (test6.mFinished) {
					break;
				} else {
					Thread.sleep(100);
				}
			}*/
			
			RemoveDurableEntry test7 = new RemoveDurableEntry();
			
			while (true) {
				if (test7.mFinished) {
					break;
				} else {
					Thread.sleep(100);
				}
			}
			
			AddCallbacksAfterRestart test8 = new AddCallbacksAfterRestart();
		
			/*mCurrInstance = GSHelperMethods.createInstanceWaitForAvailable("test", new GSHelperMethods.Callback() {
				@Override
				public void function() {
					try {
						part1();				
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});*/
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
