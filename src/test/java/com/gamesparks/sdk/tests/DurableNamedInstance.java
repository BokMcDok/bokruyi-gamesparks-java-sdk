package com.gamesparks.sdk.tests;

import com.gamesparks.sdk.*;
import com.gamesparks.sdk.api.GSData;
import com.gamesparks.sdk.api.autogen.GSMessageHandler;
import com.gamesparks.sdk.api.autogen.GSRequestBuilder.*;
import com.gamesparks.sdk.api.autogen.GSMessageHandler.ScriptMessage;
import com.gamesparks.sdk.api.autogen.GSRequestBuilder.AuthenticationRequest;
import com.gamesparks.sdk.api.autogen.GSResponseBuilder.AroundMeLeaderboardResponse;
import com.gamesparks.sdk.api.autogen.GSResponseBuilder.AuthenticationResponse;
import com.gamesparks.sdk.api.autogen.GSTypes.LeaderboardData;

public class DurableNamedInstance {
	
	public boolean mFinished = false;
	
	private GSExtended mCurrInstance;
	private Object mBackendMessage = null;
	private AutoResetEvent mMessageEvent;

	public DurableNamedInstance() throws Exception {
		System.out.println("*** DurableNamedInstance ***");
		
		mCurrInstance = GSHelperMethods.createInstanceWaitForAvailable("DurableNamedInstance_1", new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part2();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part2() throws Exception {
		GSHelperMethods.waitForDeviceAuthentication(mCurrInstance, new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part3();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part3() throws Exception {
		mCurrInstance.clearDurableQueue();
		
		LogEventRequest request = GSHelperMethods.getLogRequest(mCurrInstance);
		
		mCurrInstance.setNetworkAvailable(false);
		
		mMessageEvent = new AutoResetEvent(false);
		
		mCurrInstance.getMessageHandler().setScriptMessageListener(new GSEventConsumer<GSMessageHandler.ScriptMessage>() {
			@Override
			public void onEvent(ScriptMessage event) {
				mBackendMessage = event;
				mMessageEvent.set();	
			}
		});
	
		request.setDurable(true);
		request.send(null);
		
		Assert.assertTrue(mCurrInstance.getSizePersistentQueue() > 0);
		
		GSHelperMethods.waitForShutdown(mCurrInstance, new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part4();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part4() throws Exception {
		Assert.assertNull(mBackendMessage);
		
		mCurrInstance.setNetworkAvailable(true);
		
		mCurrInstance = GSHelperMethods.createInstanceWaitForAvailable("DurableNamedInstance_1", new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part5();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part5() {
		mMessageEvent.waitOne(GSHelperMethods.REQUEST_TIMEOUT, new AutoResetEvent.Callback() {	
			@Override
			public void function(boolean timedout) throws Exception {
				part6(timedout);
			}
		});
	}
	
	private void part6(boolean timedout) throws Exception {
		Assert.assertFalse("Durable was never sent.", timedout);
		
		Assert.assertNotNull(mBackendMessage);
		
		GSHelperMethods.waitForShutdown(mCurrInstance, new GSHelperMethods.Callback() {
			@Override
			public void function() {
				part7();
			}
		});
	}
	
	private void part7() {
		GSHelperMethods.shutDownAllInstances();
		
		System.out.println("Passed!");
		
		mFinished = true;
	}
}
