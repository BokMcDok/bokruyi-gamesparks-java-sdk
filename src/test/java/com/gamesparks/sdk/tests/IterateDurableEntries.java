package com.gamesparks.sdk.tests;

import com.gamesparks.sdk.Assert;
import com.gamesparks.sdk.GSExtended;
import com.gamesparks.sdk.GSHelperMethods;
import com.gamesparks.sdk.api.autogen.GSRequestBuilder.LogEventRequest;

public class IterateDurableEntries {
	
	public boolean mFinished = false;
	
	private static final int AMOUNT_OF_REQUESTS = 3;
	
	private GSExtended mCurrInstance;
	
	public IterateDurableEntries() throws Exception {
		System.out.println("*** IterateDurableEntries ***");
		
		mCurrInstance = GSHelperMethods.createInstanceWaitForAvailable("IterateDurableEntries", new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part2();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part2() throws Exception {
		GSHelperMethods.waitForDeviceAuthentication(mCurrInstance, new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part3();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part3() throws Exception {
		int i;
		
		mCurrInstance.setNetworkAvailable(false);
		
		for (i = 0; i < AMOUNT_OF_REQUESTS; i ++) {
			LogEventRequest request = GSHelperMethods.getLogRequest(mCurrInstance);
		
			request.setDurable(true);
			request.setEventAttribute("foo", String.valueOf(i));
			request.send(null);
		}
		
		Assert.assertTrue(mCurrInstance.getSizePersistentQueue() == AMOUNT_OF_REQUESTS);
		
		for (i = 0; i < AMOUNT_OF_REQUESTS; i ++) {
			Assert.assertTrue("error foo", mCurrInstance.compareAttributeDurableRequest(i, "foo", String.valueOf(i)));
		}
		
		mCurrInstance.setNetworkAvailable(true);
		
		GSHelperMethods.waitForShutdown(mCurrInstance, new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part4();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part4() {
		GSHelperMethods.shutDownAllInstances();
		
		System.out.println("Passed!");
		
		mFinished = true;
	}
}
