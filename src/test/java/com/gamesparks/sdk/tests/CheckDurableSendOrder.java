package com.gamesparks.sdk.tests;

import java.util.ArrayList;
import java.util.List;

import com.gamesparks.sdk.Assert;
import com.gamesparks.sdk.GSEventConsumer;
import com.gamesparks.sdk.GSExtended;
import com.gamesparks.sdk.GSHelperMethods;
import com.gamesparks.sdk.api.autogen.GSMessageHandler;
import com.gamesparks.sdk.api.autogen.GSMessageHandler.ScriptMessage;
import com.gamesparks.sdk.api.autogen.GSRequestBuilder.LogEventRequest;

public class CheckDurableSendOrder {
	
	public boolean mFinished = false;
	
	private static final int AMOUNT_OF_REQUESTS = 3;

	private GSExtended mCurrInstance;
	private List<Object> mSentEvents = new ArrayList<>();
	
	public CheckDurableSendOrder() throws Exception {
		System.out.println("*** CheckDurableSendOrder ***");
		
		mCurrInstance = GSHelperMethods.createInstanceWaitForAvailable("CheckDurableSendOrder", new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part2();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part2() throws Exception {
		GSHelperMethods.waitForDeviceAuthentication(mCurrInstance, new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part3();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part3() throws Exception {
		int i;
		
		mCurrInstance.setNetworkAvailable(false);
		
		for (i = 0; i < AMOUNT_OF_REQUESTS; i ++) {
			LogEventRequest request = GSHelperMethods.getLogRequest(mCurrInstance);
		
			request.setDurable(true);
			request.setEventAttribute("foo", String.valueOf(i));
			request.send(null);
		}
		
		mCurrInstance.getMessageHandler().setScriptMessageListener(new GSEventConsumer<GSMessageHandler.ScriptMessage>() {
			@Override
			public void onEvent(ScriptMessage event) {
				Object value = event.getData().getAttribute("foo");
				
				if (value != null) {
					mSentEvents.add(value);
				}
			}
		});
		
		mCurrInstance.setNetworkAvailable(true);
		
		GSHelperMethods.waitForShutdown(mCurrInstance, new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part4();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part4() throws Exception {
		int i;
		
		GSHelperMethods.shutDownAllInstances();
		
		Assert.assertTrue(mSentEvents.size() == AMOUNT_OF_REQUESTS);
		
		for (i = 0; i < mSentEvents.size(); i ++)
		{
			Assert.assertEquals(mSentEvents.get(i), String.valueOf(i));
		}
		
		System.out.println("Passed!");
		
		mFinished = true;
	}
}
