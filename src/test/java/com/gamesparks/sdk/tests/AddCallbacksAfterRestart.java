package com.gamesparks.sdk.tests;

import com.gamesparks.sdk.Assert;
import com.gamesparks.sdk.AutoResetEvent;
import com.gamesparks.sdk.GSEventConsumer;
import com.gamesparks.sdk.GSExtended;
import com.gamesparks.sdk.GSHelperMethods;
import com.gamesparks.sdk.api.autogen.GSRequestBuilder.LogEventRequest;
import com.gamesparks.sdk.api.autogen.GSResponseBuilder.LogEventResponse;

public class AddCallbacksAfterRestart {
	
	public boolean mFinished = false;

	private GSExtended mCurrInstance;
	
	public AddCallbacksAfterRestart() throws Exception {
		System.out.println("*** AddCallbacksAfterRestart ***");
		
		mCurrInstance = GSHelperMethods.createInstanceWaitForAvailable("AddCallbacksAfterRestart", new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part2();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part2() throws Exception {
		mCurrInstance.setDurableQueuePaused(true);
		
		GSHelperMethods.waitForDeviceAuthentication(mCurrInstance, new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part3();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part3() throws Exception {
		mCurrInstance.clearDurableQueue();
		
		LogEventRequest request = GSHelperMethods.getLogRequest(mCurrInstance);
		
		mCurrInstance.setNetworkAvailable(false);
		
		request.setDurable(true);
		request.send(null);
		
		Assert.assertTrue(mCurrInstance.getSizePersistentQueue() == 1);
		
		GSHelperMethods.waitForShutdown(mCurrInstance, new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part4();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part4() throws Exception {
		mCurrInstance.setNetworkAvailable(true);
		
		mCurrInstance = GSHelperMethods.createInstanceAndConnect("AddCallbacksAfterRestart");
		
		mCurrInstance.setDurableQueuePaused(false);
		
		final AutoResetEvent callbackEvent = new AutoResetEvent(false);
		
		mCurrInstance.setOnAvailable(new GSEventConsumer<Boolean>() {
			@Override
			public void onEvent(Boolean available) {
				if (available) {
					mCurrInstance.setCallbackDurableRequest(0, new GSEventConsumer<LogEventResponse>() {
				        @Override
				        public void onEvent(LogEventResponse response) {
				        	callbackEvent.set();
				        }
				    });
				}
			}
		});
		
		callbackEvent.waitOne(10000, new AutoResetEvent.Callback() {	
			@Override
			public void function(boolean timedout) throws Exception {
				part5(timedout);
			}
		});
	}
	
	private void part5(boolean timedout) throws Exception {
		Assert.assertFalse("Callback not called after durable restart.", timedout);
		
		GSHelperMethods.waitForShutdown(mCurrInstance, new GSHelperMethods.Callback() {
			@Override
			public void function() {
				part6();
			}
		});
	}
	
	private void part6() {
		GSHelperMethods.shutDownAllInstances();
		
		System.out.println("Passed!");
		
		mFinished = true;
	}
}
