package com.gamesparks.sdk.tests;

import com.gamesparks.sdk.Assert;
import com.gamesparks.sdk.GSExtended;
import com.gamesparks.sdk.GSHelperMethods;
import com.gamesparks.sdk.api.autogen.GSRequestBuilder.LogEventRequest;

public class RemoveDurableEntry {
	
	public boolean mFinished = false;

	private GSExtended mCurrInstance;
	
	public RemoveDurableEntry() throws Exception {
		System.out.println("*** RemoveDurableEntry ***");
		
		mCurrInstance = GSHelperMethods.createInstanceWaitForAvailable("RemoveDurableEntry", new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part2();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part2() throws Exception {
		GSHelperMethods.waitForDeviceAuthentication(mCurrInstance, new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part3();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part3() throws Exception {
		mCurrInstance.setNetworkAvailable(false);
		
		LogEventRequest request = GSHelperMethods.getLogRequest(mCurrInstance);
		
		request.setDurable(true);
		request.send(null);
		
		Assert.assertTrue(mCurrInstance.getSizePersistentQueue() == 1);
		
		mCurrInstance.removeDurableRequest(0);
		
		Assert.assertTrue(mCurrInstance.getSizePersistentQueue() == 0);
		
		mCurrInstance.setNetworkAvailable(true);
		
		GSHelperMethods.waitForShutdown(mCurrInstance, new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part4();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	private void part4() {
		GSHelperMethods.shutDownAllInstances();
		
		System.out.println("Passed!");
		
		mFinished = true;
	}
}
