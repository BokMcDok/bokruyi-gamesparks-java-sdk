package com.gamesparks.sdk.tests;

import com.gamesparks.sdk.Assert;
import com.gamesparks.sdk.AutoResetEvent;
import com.gamesparks.sdk.GSEventConsumer;
import com.gamesparks.sdk.GSExtended;
import com.gamesparks.sdk.GSHelperMethods;
import com.gamesparks.sdk.api.autogen.GSMessageHandler;
import com.gamesparks.sdk.api.autogen.GSResponseBuilder;
import com.gamesparks.sdk.api.autogen.GSResponseBuilder.LogEventResponse;
import com.gamesparks.sdk.api.autogen.GSMessageHandler.ScriptMessage;
import com.gamesparks.sdk.api.autogen.GSRequestBuilder.LogEventRequest;

public class RestartDurableQueue {
	
	public boolean mFinished = false;

	private GSExtended mCurrInstance;
	private LogEventResponse mLogResponse = null;
	private AutoResetEvent mLogResponseEvent;
	
	public RestartDurableQueue() throws Exception {
		System.out.println("*** RestartDurableQueue ***");
		
		mCurrInstance = GSHelperMethods.createInstanceWaitForAvailable("RestartDurableQueue", new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part2();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part2() throws Exception {
		mCurrInstance.setDurableQueuePaused(true);
		
		GSHelperMethods.waitForDeviceAuthentication(mCurrInstance, new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part3();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part3() throws Exception {
		mCurrInstance.clearDurableQueue();
		
		LogEventRequest request = GSHelperMethods.getLogRequest(mCurrInstance);
		
		mLogResponseEvent = new AutoResetEvent(false);
		
		request.setDurable(true);
		request.send(new GSEventConsumer<LogEventResponse>() {
	        @Override
	        public void onEvent(LogEventResponse response) {
	        	mLogResponse = response;
				mLogResponseEvent.set();
	        }
	    });
		
		mLogResponseEvent.waitOne(GSHelperMethods.REQUEST_TIMEOUT, new AutoResetEvent.Callback() {	
			@Override
			public void function(boolean timedout) throws Exception {
				part4(timedout);
			}
		});
	}
	
	private void part4(boolean timedout) throws Exception {
		Assert.assertTrue("Durable was sent, when it should not.", timedout);
		
		Assert.assertNull(mLogResponse);
		
		Assert.assertTrue(mCurrInstance.getSizePersistentQueue() == 1);
		
		mCurrInstance.setDurableQueuePaused(false);
		
		mLogResponseEvent.waitOne(GSHelperMethods.REQUEST_TIMEOUT, new AutoResetEvent.Callback() {	
			@Override
			public void function(boolean timedout) throws Exception {
				part5(timedout);
			}
		});
	}
	
	private void part5(boolean timedout) throws Exception {
		Assert.assertFalse("Timeout. Durable was not sent.", timedout);
		
		GSHelperMethods.waitForShutdown(mCurrInstance, new GSHelperMethods.Callback() {
			@Override
			public void function() {
				part6();
			}
		});
	}
	
	private void part6() {
		GSHelperMethods.shutDownAllInstances();
		
		System.out.println("Passed!");
		
		mFinished = true;
	}
}
