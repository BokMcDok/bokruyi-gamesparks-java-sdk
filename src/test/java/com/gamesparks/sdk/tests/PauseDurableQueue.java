package com.gamesparks.sdk.tests;

import com.gamesparks.sdk.Assert;
import com.gamesparks.sdk.GSExtended;
import com.gamesparks.sdk.GSHelperMethods;
import com.gamesparks.sdk.api.autogen.GSRequestBuilder.LogEventRequest;

public class PauseDurableQueue {
	
	public boolean mFinished = false;

	private GSExtended mCurrInstance;
	
	public PauseDurableQueue() throws Exception {
		System.out.println("*** PauseDurableQueue ***");
		
		mCurrInstance = GSHelperMethods.createInstanceWaitForAvailable("PauseDurableQueue", new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part2();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part2() throws Exception {
		GSHelperMethods.waitForDeviceAuthentication(mCurrInstance, new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part3();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part3() throws Exception {
		mCurrInstance.clearDurableQueue();
		
		mCurrInstance.setDurableQueuePaused(true);
		
		LogEventRequest request = GSHelperMethods.getLogRequest(mCurrInstance);
		
		mCurrInstance.setNetworkAvailable(false);
	
		request.setDurable(true);
		request.send(null);
		
		Assert.assertTrue(mCurrInstance.getSizePersistentQueue() == 1);
		
		GSHelperMethods.waitForShutdown(mCurrInstance, new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part4();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part4() throws Exception {
		mCurrInstance.setNetworkAvailable(true);
		
		mCurrInstance = GSHelperMethods.createInstanceWaitForAvailable("PauseDurableQueue", new GSHelperMethods.Callback() {
			@Override
			public void function() {
				try {
					part5();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void part5() throws Exception {
		Assert.assertTrue(mCurrInstance.getSizePersistentQueue() == 1);
		
		mCurrInstance.setDurableQueuePaused(false);
		
		GSHelperMethods.waitForShutdown(mCurrInstance, new GSHelperMethods.Callback() {
			@Override
			public void function() {
				part6();
			}
		});
	}
	
	private void part6() {
		GSHelperMethods.shutDownAllInstances();
		
		System.out.println("Passed!");
		
		mFinished = true;
	}
}
